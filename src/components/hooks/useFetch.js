import React, { useEffect, useState } from 'react'

const useFetch = (url) => {
    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);

    const FetchData = async (url) => {
        setIsLoading(true);

        try{
            const response = await fetch(url);
            const resData = await response.json();
            if(!response.ok){
                throw resData;
            }
            //setTimeout(()=>{
                setData(resData);
                setIsLoading(false);
                setError(null);

            //},1000)
        }catch(err){
            setIsLoading(false);
            setError(err);
        }
    }

    useEffect(()=>{
        FetchData(url)
    },[url])
    return {data, isLoading, error};
}

export default useFetch