
const bookReducer = (state, action) => {
    if(action.type === "ADD"){
        const allBooks = [...state.books, action.payload];
        return {
            ...state,
            books : allBooks,
            isModalOpen : true,
            modalText : "New book added successfully.",
            isColorSuccess : true

        }
    }
    if(action.type === "REMOVE"){
        const allBooks = state.books.filter((book)=> book.id !== action.payload);
        return {
            ...state,
            books : allBooks,
            isModalOpen : true,
            modalText : "The book is removed successfully.",
            isColorSuccess : false

        }
    }
    
    if(action.type === "EDIT"){
        const allBooks = state.books.map((book) => {
            if(book.id === action.payload.id){
                book.name = action.payload.name;
            }
            return book;
        });
        return {
            ...state,
            books : allBooks,
            isModalOpen : true,
            modalText : "The book is edited successfully.",
            isColorSuccess : true

        }
    } 
    
    if(action.type === "TOGGLE"){
        return {
            ...state,
            isModalOpen : !action.payload,
            modalText : ""

        }
    }
    
    return {
        ...state,
        isModalOpen : true,
        modalText : "Please write a book name .",
        isColorSuccess : false

    }; 
};

export default bookReducer;