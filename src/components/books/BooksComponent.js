import React, { useState } from 'react'
import {v4 as uuidv4} from 'uuid'
import {Button, Container, Form, Table, Modal, InputGroup, Row, Col } from 'react-bootstrap';

const booksData = [
    {id : uuidv4(), name : "Pather Panchal"},
    {id : uuidv4(), name : "Padma Nadir Majhi"},
    {id : uuidv4(), name : "Srikanta"},
];

const ModalComponent = ({isColorSuccess, modalText, isModalOpen, isModalClose}) =>{

    const [show, setShow] = useState(isModalOpen);
    const handleClose = () => {
        setShow(false);
        isModalClose()
    }

    return (
        <>
        <Modal show={show} onHide={handleClose} animation={true} className='outline-primary'>
            <Modal.Header closeButton>
            <Modal.Title className={isColorSuccess ? 'text-success' : 'text-danger'}>Message</Modal.Title>
            </Modal.Header>
            <Modal.Body className={isColorSuccess ? 'text-success' : 'text-danger'}>{modalText}</Modal.Body>
            <Modal.Footer>
            <Button variant="outline-primary" size='sm' onClick={handleClose}>
                OK
            </Button>
            </Modal.Footer>
        </Modal>
        </>
    );

}

const BooksComponent = () => {
    const [books, setBooks] = useState(booksData);
    const [isModalOpen, setIsModalOpen] =useState(false);
    const [modalText, setModalText] = useState(null);
    const [bookName, setBookName] = useState("");
    const [bookId, setBookId] = useState("");
    const [isColorSuccess, setIsColorSuccess] = useState(false);

    const handleFormData = (event) => {
        event.preventDefault();
        if(bookName !== ""){
            if(bookId === ""){
                const newBook = { id : uuidv4(), name : bookName};
                setBooks((prevState)=>{
                    return [...prevState, newBook];
                })
                setModalText("New book added successfully.")
            }
            else{
                books.map((book) => {
                    if(book.id === bookId){
                        book.name = bookName;
                    }
                });
                setModalText("The book is edited successfully.")
            }
            setIsColorSuccess(true)
            setIsModalOpen(true);
            setBookName("");
            setBookId("")

        }
        else{
            setIsColorSuccess(false)
            setModalText("Please write a book name .")
            setIsModalOpen(true);
            setBookName("");

        }
        
    }

    const handleRemoveBook = (bookID) =>{
        const filteredData = books.filter((book)=> book.id !== bookID);
        setBooks(filteredData);
        setIsColorSuccess(false)
        setModalText("The book is removed successfully.")
        setIsModalOpen(true);
    }
    
    const handleEditBook = ({id, name}) =>{
        setBookName(name);
        setBookId(id)
    }

  return (
    <>
        {
            isModalOpen &&
            <ModalComponent isColorSuccess={isColorSuccess} modalText={modalText} isModalOpen={isModalOpen} isModalClose={()=>setIsModalOpen(false)}/>
        }
        
        <Container className='text-center'>
            <Form onSubmit={handleFormData} >
                <InputGroup>
                    <InputGroup.Text htmlFor='bookName' size="sm"> Book Name : </InputGroup.Text>
                    <Form.Control type='text' name='bookName' size='sm' value={bookName} onChange={(e) => setBookName(e.target.value)} placeholder="Enter Book Name"/>
                    <Button className='col-sm-2 col-md-2 col-lg-2' variant='outline-success' type='submit' size='sm'>Save</Button>
                </InputGroup>
            </Form>
            <h3 className='appTitle p-2  mt-4'><strong>BOOK LIST</strong></h3>
            <Table responsive striped hover border={1} size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Book Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        books &&
                        books.map((book,index)=>{
                            return <tr key={index+1}>
                                <td>{(index+1)?.toString().padStart(2, '0')}</td>
                                <td>{book.name}</td>
                                <td>
                                    <Button variant='outline-primary' size='sm' onClick={() => handleEditBook(book)}><i className='fa fa-edit'></i></Button>
                                    <Button variant='outline-danger' size='sm' onClick={() => handleRemoveBook(book.id)}><i className='fa fa-trash'></i></Button>
                                </td>
                            </tr>
                        })
                    }
                </tbody>
            </Table>
        </Container>

    </>
  )
}

export default BooksComponent