import React, { useEffect, useReducer, useState } from 'react'
import {Button, Container, Form, Table, InputGroup} from 'react-bootstrap';
import {v4 as uuidv4} from 'uuid'

import bookReducer from '../hooks/bookReducer';
import ModalComponent from '../common/ModalComponent'

const booksData = [
    {id : uuidv4(), name : "Pather Panchal"},
    {id : uuidv4(), name : "Padma Nadir Majhi"},
    {id : uuidv4(), name : "Srikanta"},
];


const BooksReducer = () => {

    const [bookState, dispatch] = useReducer(bookReducer, {
        books : booksData,
        isModalOpen : false,
        modalText : null,
        isColorSuccess : false
    })
    
    const [bookName, setBookName] = useState("");
    const [bookId, setBookId] = useState("");

    useEffect(()=>{
        console.log(bookState);
    })

    const handleFormData = (event) => {
        event.preventDefault();
        if(bookName !== ""){
            if(bookId === ""){
                const newBook = { id : uuidv4(), name : bookName};
                dispatch({type : "ADD", payload :newBook });
            }
            else{
                const book = { id : bookId, name : bookName};
                dispatch({type : "EDIT", payload : book})
                setBookId("")
            }

        }
        else{
            dispatch({type : "invalid", payload : ""});

        }
        setBookName("");
        
    }

    const handleRemoveBook = (bookID) =>{
        dispatch({type : "REMOVE", payload : bookID})
    }

    const toggleIsModalOpen = () =>{
        dispatch({type : "TOGGLE", payload : bookState.isModalOpen})
    }
    
    const handleEditBook = (book) =>{
        setBookId(book.id)
        setBookName(book.name)
    }

  return (
    <>
        {
            bookState.isModalOpen &&
            <ModalComponent isColorSuccess={bookState.isColorSuccess} modalText={bookState.modalText} isModalOpen={bookState.isModalOpen} isModalClose={toggleIsModalOpen}/>
        }
        
        <Container className='text-center'>
            <Form onSubmit={handleFormData} >
                <InputGroup>
                    <InputGroup.Text htmlFor='bookName' size="sm"> Book Name : </InputGroup.Text>
                    <Form.Control type='text' name='bookName' size='sm' value={bookName} onChange={(e) => setBookName(e.target.value)} placeholder="Enter Book Name"/>
                    <Button className='col-sm-2 col-md-2 col-lg-2' variant='outline-success' type='submit' size='sm'>Save</Button>
                </InputGroup>
            </Form>
            <h3 className='appTitle p-2  mt-4'><strong>BOOK LIST</strong></h3>
            <Table responsive striped hover border={1} size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Book Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        bookState.books &&
                        bookState.books.map((book,index)=>{
                            return <tr key={index+1}>
                                <td>{(index+1)?.toString().padStart(2, '0')}</td>
                                <td>{book.name}</td>
                                <td>
                                    <Button variant='outline-primary' size='sm' onClick={() => handleEditBook(book)}><i className='fa fa-edit'></i></Button>
                                    <Button variant='outline-danger' size='sm' onClick={() => handleRemoveBook(book.id)}><i className='fa fa-trash'></i></Button>
                                </td>
                            </tr>
                        })
                    }
                </tbody>
            </Table>
        </Container>

    </>
  )
}

export default BooksReducer