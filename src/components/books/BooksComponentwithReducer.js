import React, { useEffect, useReducer, useState } from 'react'
import {v4 as uuidv4} from 'uuid'
import {Button, Container, Form, Table, Modal, InputGroup} from 'react-bootstrap';

const booksData = [
    {id : uuidv4(), name : "Pather Panchal"},
    {id : uuidv4(), name : "Padma Nadir Majhi"},
    {id : uuidv4(), name : "Srikanta"},
];

const ModalComponent = ({isColorSuccess, modalText, isModalOpen, isModalClose}) =>{

    const [show, setShow] = useState(isModalOpen);
    const handleClose = () => {
        setShow(false);
        isModalClose()
    }

    return (
        <>
        <Modal show={show} onHide={handleClose} animation={true} className='outline-primary'>
            <Modal.Header closeButton>
            <Modal.Title className={isColorSuccess ? 'text-success' : 'text-danger'}>Message</Modal.Title>
            </Modal.Header>
            <Modal.Body className={isColorSuccess ? 'text-success' : 'text-danger'}>{modalText}</Modal.Body>
            <Modal.Footer>
            <Button variant="outline-primary" size='sm' onClick={handleClose}>
                OK
            </Button>
            </Modal.Footer>
        </Modal>
        </>
    );

}

const bookReducer = (state, action) => {
    if(action.type === "ADD"){
        const allBooks = [...state.books, action.payload];
        return {
            ...state,
            books : allBooks,
            isModalOpen : true,
            modalText : "New book added successfully.",
            isColorSuccess : true

        }
    }
    if(action.type === "REMOVE"){
        const allBooks = state.books.filter((book)=> book.id !== action.payload);
        return {
            ...state,
            books : allBooks,
            isModalOpen : true,
            modalText : "The book is removed successfully.",
            isColorSuccess : false

        }
    }
    
    if(action.type === "EDIT"){
        const allBooks = state.books.map((book) => {
            if(book.id === action.payload.id){
                book.name = action.payload.name;
            }
            return book;
        });
        return {
            ...state,
            books : allBooks,
            isModalOpen : true,
            modalText : "The book is edited successfully.",
            isColorSuccess : true

        }
    } 
    
    if(action.type === "TOGGLE"){
        return {
            ...state,
            isModalOpen : !action.payload,
            modalText : ""

        }
    }
    
    return {
        ...state,
        isModalOpen : true,
        modalText : "Please write a book name .",
        isColorSuccess : false

    }; 
}


const AppBookReducer = () => {

    const [bookState, dispatch] = useReducer(bookReducer, {
        books : booksData,
        isModalOpen : false,
        modalText : null,
        isColorSuccess : false
    })
    
    const [bookName, setBookName] = useState("");
    const [bookId, setBookId] = useState("");

    useEffect(()=>{
        console.log(bookState);
    })

    const handleFormData = (event) => {
        event.preventDefault();
        if(bookName !== ""){
            if(bookId === ""){
                const newBook = { id : uuidv4(), name : bookName};
                dispatch({type : "ADD", payload :newBook });
            }
            else{
                const book = { id : bookId, name : bookName};
                dispatch({type : "EDIT", payload : book})
                setBookId("")
            }

        }
        else{
            dispatch({type : "invalid", payload : ""});

        }
        setBookName("");
        
    }

    const handleRemoveBook = (bookID) =>{
        dispatch({type : "REMOVE", payload : bookID})
    }

    const toggleIsModalOpen = () =>{
        dispatch({type : "TOGGLE", payload : bookState.isModalOpen})
    }
    
    const handleEditBook = (book) =>{
        setBookId(book.id)
        setBookName(book.name)
    }

  return (
    <>
        {
            bookState.isModalOpen &&
            <ModalComponent isColorSuccess={bookState.isColorSuccess} modalText={bookState.modalText} isModalOpen={bookState.isModalOpen} isModalClose={toggleIsModalOpen}/>
        }
        
        <Container className='text-center'>
            <Form onSubmit={handleFormData} >
                <InputGroup>
                    <InputGroup.Text htmlFor='bookName' size="sm"> Book Name : </InputGroup.Text>
                    <Form.Control type='text' name='bookName' size='sm' value={bookName} onChange={(e) => setBookName(e.target.value)} placeholder="Enter Book Name"/>
                    <Button className='col-sm-2 col-md-2 col-lg-2' variant='outline-success' type='submit' size='sm'>Save</Button>
                </InputGroup>
            </Form>
            <h3 className='appTitle p-2  mt-4'><strong>BOOK LIST</strong></h3>
            <Table responsive striped hover border={1} size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Book Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        bookState.books &&
                        bookState.books.map((book,index)=>{
                            return <tr key={index+1}>
                                <td>{(index+1)?.toString().padStart(2, '0')}</td>
                                <td>{book.name}</td>
                                <td>
                                    <Button variant='outline-primary' size='sm' onClick={() => handleEditBook(book)}><i className='fa fa-edit'></i></Button>
                                    <Button variant='outline-danger' size='sm' onClick={() => handleRemoveBook(book.id)}><i className='fa fa-trash'></i></Button>
                                </td>
                            </tr>
                        })
                    }
                </tbody>
            </Table>
        </Container>

    </>
  )
}

export default AppBookReducer