import React, { useEffect, useState } from 'react';
import useFetch from '../hooks/useFetch';
import Country from './Country';
import {Container } from 'react-bootstrap';

const url = "https://restcountries.com/v3.1/all";


const Countries = () => {
    const {data, isLoading, error} = useFetch(url);
    const [countries, setCountries] = useState(null);

    useEffect (()=>{
        setCountries(data);
    },[data]);

    const haandleSearch = (e) =>{
        const value = e.target.value?.toString().toLowerCase();
        const searchData = data.filter((country)=>{
            const countryName = country.name.common?.toString().toLowerCase();
            return countryName?.startsWith(value);
        })
        setCountries(searchData);
    }

    const handleRemoveData = (name) =>{
        const searchData = countries.filter((country)=> country.name.common?.toString().toLowerCase() !== name.toString().toLowerCase())
        setCountries(searchData);
    }

    
    return (
        
        <Container fluid>
            <div className="form-group row text-center justify-content-center">
                <div className="col-sm-4">
                        <input type='text' name="seearchCountry" onChange={haandleSearch} placeholder="Search Country by Name..." className='form form-control form-control-plaintext sm m-2 p-2 border border-primary' />
                </div>
            </div>
            <br />
            
            <div className="countries">
                {isLoading && <h2>Data is loading...</h2>}
                {error && <h2>{error.message}</h2>}
                {
                    countries &&
                    countries.map((country, index) => <Country key={index} country={country} onRemoveData = {handleRemoveData} />)
               }
            </div>
        </Container>
    )
}

export default Countries