import React from 'react'
import { Card, Button } from 'react-bootstrap'

const Country = (props) => {
    const {name, flags, capital, population , area} = props.country;
    //console.log(name.common);
    const removeData = (name) =>{
        props.onRemoveData(name)
    }
  return (
        <Card style={{width:"20rem"}}>
            <Card.Img src={flags.png} alt={name.common} width="50px;" />
            <Card.Body>
                <Card.Title>{name.common}</Card.Title>
                <Card.Text> <strong>{capital}</strong> is the capital of <strong>{name.common}</strong> where a lots of people are living together. Around <strong>{population}</strong>  people are living in <strong>{area}</strong> sqr ft. land of {name.common}. </Card.Text>
            </Card.Body>
            <Card.Footer className='m-0 p-0 text-end'>
                <span className='me-2 pe-2 text-primary' style={{fontSize : "10.5px"}}>Btech Academy</span>
                <Button variant='outline-danger' size='sm' onClick={() => removeData(name.common)}><i className='fa fa-trash'></i></Button>
            </Card.Footer>

        </Card>
  )
}

export default Country