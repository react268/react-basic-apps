import React, { useState } from 'react'
import NewUser from './NewUserForm'
import UserTable from './UserTableComponent';

const UserComponent = () => {
    const [users, setUsers] = useState([]);

    const handleUserData = (newUser) =>{
        setUsers([...users, newUser]);
    }

    const handleDeleteUser = (userID) =>{
        
        const filterData = users.filter((user) => {
            return userID !== user.id
        });
        setUsers([...filterData]);

    }

    const handleEditUser = (userID) =>{
        const editUser = users.filter((user) => {
            return userID === user.id
        });

    }

  return (
    <div>
        <NewUser onNewData={handleUserData} />
        <UserTable users = {users} onDeleteData = {handleDeleteUser} onEditData = {handleEditUser} />
    </div>
  )
}

export default UserComponent