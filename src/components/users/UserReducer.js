import React, {useReducer, useState} from 'react'
import { Button, Container, Form, Row, Col, Table, Alert } from 'react-bootstrap';
import {v4 as uuidv4} from 'uuid';

const useUserReducer = (state, action) => {
    const type = action.type;
    switch(type){
        case "ADD":
            const allUsers = [...state.users, action.payload];
            return {
                ...state,
                users : allUsers,
                isModalOpen : true,
                modalText : "New user added successfully.",
                modalTextColor : "success"
            }
        case "EDIT":
            const fileterUsers = state.users.map((user)=>{
                if(user.id === action.payload.id){
                    user = action.payload
                }
                return user;
            })
            return {
                ...state,
                users : fileterUsers,
                isModalOpen : true,
                modalText : "The user information is updated successfully.",
                modalTextColor : "warning"
            }
        case "REMOVE":
            const fileterUserData = state.users.filter((user)=> user.id !== action.payload);
            return {
                ...state,
                users : fileterUserData,
                isModalOpen : true,
                modalText : "The user is removed successfully.",
                modalTextColor : "danger"

            }
        case "TOGGLE":
            return {
                ...state,
                isModalOpen : !action.payload,
                modalText : "",
                modalTextColor : ""
    
            }
        default:
            return {
                ...state
            }

    }


}

const userStateData = {
    users : [],
    isModalOpen : false,
    modalText : "",
    modalTextColor : ""
  };

  const newUserData = {
    id : "",
    fname: "",
    lname: "",
    phone : "",
    email : "",
    presentAddr : ""
  }

const UserReducer = () => {
  const [userState, dispatch] = useReducer(useUserReducer, userStateData)
  const [newUser, setNewUser] = useState(newUserData);

  const handleFormData = (e) => {
    e.preventDefault();
    if(newUser.id === ""){
        dispatch({type: "ADD", payload : {...newUser, id: uuidv4()}})
    }
    else{
        dispatch({type: "EDIT", payload : newUser})
    }
    setNewUser(newUserData)

  }

  const onChangeData = (e) => {
    setNewUser({...newUser, [e.target.name] : e.target.value});
  }

  const onClickResetButton = (e) => {
    setNewUser(newUserData);
  }

  const handleEditData = (user) => {
    console.log(user);
    setNewUser({...user});
  }

  const handleDeleteData = (userId) => {
    dispatch({type: "REMOVE", payload: userId})
  }
  
  const toggleIsModalOpen = () =>{
    dispatch({type : "TOGGLE", payload : userState.isModalOpen})
  }

  return (
    <>
    <Container>
        <p style={{color:"white", backgroundColor: "#766725", padding:"5px", textAlign: "center"}}>Add New User</p>
        {
            userState.isModalOpen &&
            <Alert variant={userState.modalTextColor} dismissible onClose={toggleIsModalOpen}>
                {userState.modalText}
          </Alert>
        }
        <Form onSubmit={handleFormData} >
            <Form.Control type="hidden" name='id' value={newUser.id} />
            <Row>
                <Col>
                    <Form.Group controlId="fname">
                        <Form.Label><strong>First Name :</strong></Form.Label>
                        <Form.Control size='sm' type="text" name='fname' value={newUser.fname} onChange={onChangeData} placeholder="Enter First Name" />
                    </Form.Group>
                </Col>
                <Col>
                    <Form.Group controlId="lname">
                        <Form.Label ><strong>Last Name :</strong></Form.Label>
                        <Form.Control size='sm' type="text" name='lname' value={newUser.lname} onChange={onChangeData} placeholder="Enter Last Name" />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form.Group controlId="phone">
                        <Form.Label ><strong>Phone :</strong></Form.Label>
                        <Form.Control size='sm' type="text" name='phone' value={newUser.phone} onChange={onChangeData} placeholder="Enter Your Phone Number" />
                    </Form.Group>
                </Col>
                <Col>  
                    <Form.Group controlId="email">
                        <Form.Label ><strong>Email :</strong></Form.Label>
                        <Form.Control size='sm' type="email" name='email' value={newUser.email} onChange={onChangeData} placeholder="Enter Your Email" />
                    </Form.Group>
                </Col>
            </Row>
            <Form.Group controlId="presentAddr" className='mb-3'>
                <Form.Label ><strong>Present Address :</strong></Form.Label>
                <Form.Control size='sm' type="text" name='presentAddr' value={newUser.presentAddr} onChange={onChangeData} placeholder="Enter Your Present Address" />
            </Form.Group>

            <Button type='submit' variant='primary' size='sm'>Save</Button>
            <Button type='reset' variant='danger' size='sm' onClick={onClickResetButton}>Reset</Button>
        
        </Form>
    </Container>
        <Container>
            <Table responsive hover striped className='mt-5' size='sm'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        userState.users &&
                        userState.users.map((user, index)=>{
                            return <tr key={user.id}>
                                <td>{(index+1).toString().padStart(2, '0')}</td>
                                <td>{user.fname} {user.lname}</td>
                                <td>{user.phone}</td>
                                <td>{user.email}</td>
                                <td>{user.presentAddr}</td>
                                <td>
                                    <Button variant='outline-primary' size='sm' onClick={()=>handleEditData(user)}><i className='fa fa-edit'></i></Button>
                                    <Button variant='outline-danger' size='sm' onClick={()=>handleDeleteData(user.id)}><i className='fa fa-trash'></i></Button>
                                </td>
                            </tr>;
                        })
                    }
                </tbody>
            </Table>
        </Container>
    </>
  )
}

export default UserReducer
