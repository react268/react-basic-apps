import React, { useState } from 'react'
import {Button, Modal} from 'react-bootstrap';

const ModalComponent = ({isColorSuccess, modalText, isModalOpen, isModalClose}) =>{

    const [show, setShow] = useState(isModalOpen);
    const handleClose = () => {
        setShow(false);
        isModalClose()
    }

    return (
        <>
        <Modal show={show} onHide={handleClose} animation={true} className='outline-primary'>
            <Modal.Header closeButton>
            <Modal.Title className={isColorSuccess ? 'text-success' : 'text-danger'}>Message</Modal.Title>
            </Modal.Header>
            <Modal.Body className={isColorSuccess ? 'text-success' : 'text-danger'}>{modalText}</Modal.Body>
            <Modal.Footer>
            <Button variant="outline-primary" size='sm' onClick={handleClose}>
                OK
            </Button>
            </Modal.Footer>
        </Modal>
        </>
    );

}

export default ModalComponent;