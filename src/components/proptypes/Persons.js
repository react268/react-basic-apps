import React, { useState } from 'react'
import Person from './Person'

const Persons = () => {
  const [person, setPerson] = useState({id : "0101", name: "Ahsan Ali"});
  return (
    <div>
        <Person person={person} />
    </div>
  )
}

export default Persons