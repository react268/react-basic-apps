import React from 'react'
import PropTypes from 'prop-types';

const Person = (props) => {
console.log(props.person);
  return (
    <div>
        <h3>{props.person.name}</h3>
        <p>{props.person.id}</p>
    </div>
  )
}

export default Person

Person.propTypes = {
    person : PropTypes.shape({
        id : PropTypes.string,
        name : PropTypes.string
    }),
} 