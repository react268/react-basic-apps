import React, { useContext } from 'react'
import { UserContext } from '../hooks/UserContext'

const ComponentFour = () => {
    const userData = useContext(UserContext);
    console.log("userData : ")
    console.log(userData);
  return (
    <div>
        <h3>{userData.name}</h3>
        <p>{userData.nationality}</p>
    </div>
  )
}

export default ComponentFour