import React from 'react'
import { UserContext } from '../hooks/UserContext'
import ComponentTwo from './ComponentTwo'

const CompnentOne = () => {
    const userData = {id : 1, name : "Anisul Islam", nationality : "Bangladeshi"}
  return (
    <UserContext.Provider value={userData} >
        <ComponentTwo/>
    </UserContext.Provider>
  )
}

export default CompnentOne