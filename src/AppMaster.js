import React from 'react'
import AppBookReducer from './appmasters/AppBookReducer'
import Countries from './components/countries/Countries'
import ModalComponent from './components/common/ModalComponent'
import BooksReducer from './components/books/BooksReducer'
import CompnentOne from './components/propsDrilling/CompnentOne'
import UserComponent from './components/users/UserComponent'
import UserReducer from './components/users/UserReducer'
import Persons from './components/proptypes/Persons'
import AppRoutes from './routes/AppRoutes'

const AppMaster = () => {
  return (
    <div className='m-5 p-2'>
        {/* <AppBookReducer/> */}
        {/* <ModalComponent modalText="New book added successfully." /> */}
        {/* <Countries/> */}
        {/* <BooksReducer/> */}
        {/* <CompnentOne/> */}
        {/* <UserComponent/> */}
        <UserReducer/>
        {/* <Persons/> */}
        {/* <AppRoutes/> */}

    </div>
  )
}

export default AppMaster 