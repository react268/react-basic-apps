import React from 'react';
import {v4 as uuidv4} from 'uuid';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './assets/css/custom.css';
import AppMaster from './AppMaster';


function NavbarTop (props){
  const {titleText, author} = props;
  return(
    <div>
        <h1 className='app'>
          <span className="appTitle">{titleText}</span>
          <span className="appAuthor" >{author}</span>
        </h1>
    </div>
  );
}

function App() {
  
  const titleText = "Welcome To React Basic App";
  const author = "Nazmulcs42";
  return(
    <>
    <NavbarTop titleText = {titleText} author={author} />
    <AppMaster/>
    </>
  )
}

export default App;
