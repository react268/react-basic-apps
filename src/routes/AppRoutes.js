import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Blogs from '../pages/Blogs'
import Contact from '../pages/Contact'
import Error from '../pages/Error'
import Home from '../pages/Home'
import AppNavbar from './AppNavbar'


const AppRoutes = () => {
  return (
    <BrowserRouter>
    {/* <nav>
        <ul>
            <a href='/'>Home</a> &nbsp; &nbsp;
            <a href='/blogs'>Blogs</a> &nbsp; &nbsp;
            <a href='/contact'>Contact</a> &nbsp; &nbsp;


        </ul>
    </nav> */}

    <AppNavbar/>
    
    <Routes>
        <Route path='/' element = {<Home/>} />
        <Route path='/blogs' element = {<Blogs/>} />
        <Route path='/contact' element = {<Contact/>}/>
        <Route path='*' element = {<Error/>} />
    </Routes>
        
    </BrowserRouter>
  )
}

export default AppRoutes